<?php
/*
Plugin Name: Addiction Recovery Connector
Description: A simple plugin to integrate your website with the Addiction Recovery content and publication portal.
Version: 1.0.2
Author: Addiction Recovery
Author URI: https://content.addictionrecovery.com
Requires at least: 3.5
*/

// Admin area
include( 'inc/admin.php' );

// Routes
include( 'custom_routes/class.main.php' );

// Hooks
include( 'inc/hooks.php' );

// Functions
include( 'inc/functions.php' );