<?php
// Return values stored in All In One SEO fields.	
function addiction_recovery_get_aioseo_fields( $post_id ) {
		global $wpdb;
		$table = $wpdb->prefix.'aioseo_posts';
		
		// Update the fields
		$query = "SELECT * FROM $table WHERE `post_id`=$post_id";
		$res = $wpdb->get_results( $query );
		$data = $res[0];
		$keyphrases = json_decode( $data->keyphrases );
		
		$result = array(
				"title" => $data->title,
				"description" => $data->description,
				"keyphrase" => $keyphrases->focus->keyphrase,
		);
						
		return $result;				
}

// Send request to AR Web Portal.
function addiction_recovery_send_request( $url, $token=false, $params ) {
		$headers = array(
				'Authorization' => 'Bearer ' . $token,
				'Content-Type' => 'application/json',
		);
	
		$args = array(
				'body'        => json_encode($params),
				'timeout'     => '5',
				'redirection' => '5',
				'httpversion' => '1.0',
				'blocking' => true,
				'headers'     => $headers,
				'cookies'     => array(),
				'method'     => "POST",
		);
		
		$res = wp_remote_post( $url, $args );
		$body = $res['body'];
}

// Check if Yoast SEO plugin is enabled.
function addiction_recovery_check_yoast_enabled() {
		$plugins = get_option('active_plugins');
		if( in_array( 'wordpress-seo/wp-seo.php', $plugins ) ) { return true; }
}

// Check if All In One SEO plugin is enabled.
function addiction_recovery_check_aio_enabled() {
		$plugins = get_option('active_plugins');
		if( in_array( 'all-in-one-seo-pack/all_in_one_seo_pack.php', $plugins ) ) { return true; }
}