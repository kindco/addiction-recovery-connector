<?php
add_action( 'admin_menu', 'addiction_recovery_options_page' );
function addiction_recovery_options_page() {
    add_menu_page(
        'Addiction Recovery Connector',
        'AR Connector',
        'manage_options',
        'addition_recovery_api',
        'addition_recovery_api_page_html'
    );
}

function addition_recovery_api_page_html() {
    // check user capabilities
    if ( ! current_user_can( 'manage_options' ) ) {
        return;
    }
 
    if ( isset( $_POST['settings-updated'] ) ) {
				update_option( 'addiction_recovery_custom_api_token', sanitize_text_field($_POST['addiction_recovery_custom_api_token']) );
				update_option( 'addiction_recovery_main_user', sanitize_text_field($_POST['addiction_recovery_main_user']) );
				update_option( 'addiction_recovery_web_portal_endpoint', sanitize_text_field($_POST['addiction_recovery_web_portal_endpoint']) );
				update_option( 'addiction_recovery_web_portal_token', sanitize_text_field($_POST['addiction_recovery_web_portal_token']) );
		
				// add settings saved message with the class of "updated"
				add_settings_error( 'wporg_messages', 'wporg_message', __( 'Settings Saved', 'wporg' ), 'updated' );
    }
	
		$list_users = get_users( $args );
    ?>
    <div class="wrap">
        <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
        <form action="" method="post">
		
						<input type="hidden" name="settings-updated" value="Y" />
					
						<h2><?php esc_html_e( 'Security settings' , 'Addition Recovery');?></h2>
					
						<table class="form-table" role="presentation">
								<tbody>
										<tr>
												<th scope="row"><?php esc_html_e( 'Token' , 'Addition Recovery');?></th>
												<td id="front-static-pages">
														<input type="text" name="addiction_recovery_custom_api_token" id="addiction_recovery_custom_api_token" value="<?php echo get_option('addiction_recovery_custom_api_token'); ?>" style="width: 600px;" />
														<br/>
														<a class="generate_token" href="#"><?php esc_html_e( 'Generate token' , 'Addition Recovery');?></a>
												</td>
										</tr>
							</tbody>
						</table>
					
						<h2><?php esc_html_e( 'Extra settings' , 'Addition Recovery');?></h2>
						
						<table class="form-table" role="presentation">
								<tbody>
										<tr>
												<th scope="row"><?php esc_html_e( 'The publishing user' , 'Addition Recovery');?></th>
												<td id="front-static-pages">
														<select name="addiction_recovery_main_user">
																<option><?php esc_html_e( 'Select a user' , 'Addition Recovery');?></option>
													
																<?php foreach( $list_users as $user ) { ?>
																		<option value="<?php echo $user->ID; ?>" <?php if( get_option('addiction_recovery_main_user') == $user->ID ) { ?> selected <?php } ?> > 
																				<?php echo $user->user_nicename; ?> 
																		</option>
																<?php } ?>
														</select>
												</td>
										</tr>
								</tbody>
						</table>
					
						<h2><?php esc_html_e( 'AR Web Portal settings' , 'Addition Recovery');?></h2>
					
						<table class="form-table" role="presentation">
								<tbody>
										<tr>
												<th scope="row"><?php esc_html_e( 'AR Web Portal Endpoint' , 'Addition Recovery');?></th>
												<td id="front-static-pages">
														<input type="text" name="addiction_recovery_web_portal_endpoint" id="web_portal_endpoint" value="<?php echo get_option('addiction_recovery_web_portal_endpoint'); ?>" style="width: 600px;" />
												</td>
										</tr>
										<tr>
												<th scope="row"><?php esc_html_e( 'AR Web Portal Bearer Token' , 'Addition Recovery');?></th>
												<td id="front-static-pages">
														<input type="text" name="addiction_recovery_web_portal_token" id="web_portal_token" value="<?php echo get_option('addiction_recovery_web_portal_token'); ?>" style="width: 600px;" />
												</td>
										</tr>
								</tbody>
						</table>
						
						<?php submit_button( 'Save Settings' ); ?>
						
						<div class="ref-info">
								<h2><?php esc_html_e( 'API endpoints' , 'Addition Recovery');?></h2>
							
								<p>
										<b><?php esc_html_e( 'Create/update post' , 'Addition Recovery');?>:</b> 
										your-domain/wp-json/custom_posts/v1/create/
								</p>
								<p>
										<b><?php esc_html_e( 'Get users list' , 'Addition Recovery');?>:</b> 
										your-domain/wp-json/custom_posts/v1/get_users/
								</p>
								<p>
										<b><?php esc_html_e( 'Get categories list' , 'Addition Recovery');?>:</b> 
										your-domain/wp-json/custom_posts/v1/get_categories/
								</p>
						</div>
					
						<script type="text/javascript">
								jQuery('.generate_token').click(function() {
										var token_value = generate_token(40);
										jQuery('#addiction_recovery_custom_api_token').val( token_value );
									
										return false;		
								});		
			
								function generate_token(length) {
										//edit the token allowed characters
										var a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".split("");
										var b = [];  
										for (var i=0; i<length; i++) {
												var j = (Math.random() * (a.length-1)).toFixed(0);
												b[i] = a[j];
										}
										return b.join("");
								}			
						</script>
        </form>
    </div>
<?php }